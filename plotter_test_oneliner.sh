#!/bin/bash

git clone git@bitbucket.org:dataparticles/utils.git
cd utils
git checkout plotting_tools
cp $HOME/datadir_fordsprojects/newsid_data.csv data/
printf '%s\n' "$PYTHONPATH"
export PYTHONPATH="${PYTHONPATH}:$PWD"
printf '%s\n' "$PYTHONPATH"
source activate py36danuga
python tests/plotter_tester.py
